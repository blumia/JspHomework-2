<%--
	Timetable Page.
	Check user privilege (student or teacher), query and display timetable.
	BLumia (C) 2016
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ page import="java.io.*" %>
<%@ include file="./include/userLogic.inc.jsp" %>
<%@ include file="./include/dbConn.inc.jsp" %>

<%!
	// Class struct (is that a javabean?)
	class ClassBean {
		public int weekAt = 0;
		public int orderAt = 0;
		public int length = 0;
		public String text = "<br/><br/>";

		public ClassBean(int week, int order, int len, String innerHTML) {
			weekAt = week;
			orderAt = order;
			length = len;
			text = innerHTML;
		}	
	
		public ClassBean(int week, int order) {
			this(week, order, 1, "<br/><br/>");
		}
		
		public ClassBean(int week, int order, int len) {
			this(week, order, len, "<br/><br/>");
		}
	} 
%>

<%
	// Init variables.
	String tableDOMArray[][] = new String[6][10];
	List<ClassBean> classList = new ArrayList<ClassBean>();
	
	// Query Database and push every result row into {$classList} here.
	String sql="SELECT kecheng_table.kecheng_name, class_table.class_name,teacher_table.teacher_name,classroom_table.classroom_name,xueqi,zongzhoushu,xingqi,jieciqishi,jiecijieshu FROM kebiao_table JOIN kecheng_table ON kecheng_table.kecheng_id=kebiao_table.kecheng_id JOIN class_table ON class_table.class_id= kebiao_table.class_id JOIN teacher_table ON teacher_table.teacher_id=kebiao_table.teacher_id  JOIN classroom_table ON classroom_table.classroom_id = kebiao_table.classroom_id WHERE (kebiao_table.class_id = 01)";
	ps = conn.prepareStatement(sql);
	rs = ps.executeQuery();
	while(rs.next()) {
		String tmp;
		int week = rs.getInt("xingqi");
		int order = rs.getInt("jieciqishi");
		int length = rs.getInt("jiecijieshu") - order + 1;
		tmp = rs.getString("kecheng_name") + "<br/>" + length + "节/周<br/>" + rs.getString("teacher_name") + "<br/>" + rs.getString("classroom_name");
		classList.add(new ClassBean(week,order,length,tmp));
	}
	
	// Any optional/elective class query at here.
	// Same as above, add every result to ${classList}
	
	
	// Prepare final table DOM
	for(int i = 1;i <= 5; i++) {
		for(int j = 1;j <= 9;j++) {
			tableDOMArray[i][j] = "<td week=" + i + " order=" + j + " length=1><br/><br/></td>";
		}
	}
	for(int i = 0;i < classList.size(); i ++){
		ClassBean tmp = classList.get(i);
		tableDOMArray[tmp.weekAt][tmp.orderAt] = "<td week=" + tmp.weekAt 
												+ " order=" + tmp.orderAt 
												+ " length=" + tmp.length
												+ ">" + tmp.text + "</td>";
	} 
%>

<%@ include file="./pages/timetable.jsp" %>