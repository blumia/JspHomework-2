<%--
	Front-end Page Struct Template
	BLumia (C) 2016
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
	<link rel="stylesheet" href="./w3.css">
</head>
<body>
	<%@ include file="./components/navbar.jsp" %>
	
	<% if (!hasLoggedin) { %>
	<div class="w3-padding-jumbo w3-yellow" style="margin-top: 0;">
		<h3>嘿!</h3>
		<p>右上角登陆才可以使用该系统！</p>
	</div>
		<%@ include file="./components/loginModel.jsp" %>
	<% } else { %>
	<div class="w3-padding-jumbo w3-green" style="margin-top: 0;">
		<h3>欢迎 <%=username%>!</h3>
		<p>现在就点击上面的 查看我的课表 查看你的课表吧。</p>
	</div>
	<% } %>
</body>