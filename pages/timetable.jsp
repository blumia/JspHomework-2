<%--
	Front-end Page Struct Template
	BLumia (C) 2016
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
	<link rel="stylesheet" href="./w3.css">
	<script src="http://cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
	<style>
	.w3-table th, .w3-table td{
		text-align: center;
		vertical-align: middle;
	}
	
	table, th, td {
		border: 1px solid #CCC;
	}
	</style>
</head>
<body>
	<%@ include file="./components/navbar.jsp" %>
	<div class="w3-container">
	<h1>blablabla</h1>
	<table class="w3-table w3-striped w3-bordered w3-card-4">
	<thead>
		<tr class="w3-blue">
			<th>#</th>
			<th>周一</th>
			<th>周二</th>
			<th>周三</th>
			<th>周四</th>
			<th>周五</th>
		</tr>
	</thead>
	<tbody>
	<%
		for(int row = 1; row <=9; row++) {
			out.println("<tr>");
			switch (row) {
				case 1: out.println("<td rowspan='4'>上午</td>"); break;
				case 5: out.println("<td rowspan='4'>下午</td>"); break;
				case 9: out.println("<td>晚自习</td>"); break;
			}
			for(int col = 1; col <=5; col++) {
				out.println(tableDOMArray[col][row]);
			}
			out.println("</tr>");
		}
	%>
	</tbody>
	</table>
	<br/>
	</div>

	
	<% if (!hasLoggedin) {
		response.sendRedirect("./index.jsp");
	} %>
	
<script>
// 生成表格时应为每个单元格生成其week和order属性（星期和节次），并为长度为两节的课提供属性length=2
// 这段代码会自动合并长度为2节的单元格并删除原本需要但因为合并导致的多余的单元格
$("td[length$=2]").each(function(){
	$(this).attr("rowspan",2);
	var week = $(this).attr("week");
	var order = parseInt($(this).attr("order")) + 1;
	console.log(week + order + $("td[week$="+week+"][order$="+order+"]"))
	$("td[week$="+week+"][order$="+order+"]").remove();
});
</script>
</body>