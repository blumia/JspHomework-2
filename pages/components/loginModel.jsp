<%--
	Login Model
	BLumia (C) 2016
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="login-modal" class="w3-modal">
	<span onclick="document.getElementById('login-modal').style.display='none'" class="w3-closebtn w3-hover-red w3-container w3-padding-16 w3-display-topright">&times;</span>
	<div class="w3-modal-content w3-card-8 w3-animate-zoom" style="max-width:600px">
		<div class="w3-container">
			<% if (request.getParameter("loginFailed") != null) { %>
				<div class="w3-container w3-red">
					<span onclick="this.parentElement.style.display='none'" class="w3-closebtn">×</span>
					<p>登陆失败！这年头账号密码都能输错的人不多了。</p>
				</div> 
			<% } %>
			<form action="./api/doLogin.jsp" method="POST">
			<div class="w3-section">
				<label><b>学号</b></label>
				<input name="userid" class="w3-input w3-border w3-margin-bottom" type="text" placeholder="用户名">

				<label><b>密码</b></label>
				<input name="password" class="w3-input w3-border" type="password" placeholder="输入密码">

				<button class="w3-btn w3-btn-block w3-green" type="submit">登陆</button>
				<input class="w3-check w3-margin-top" type="checkbox" checked="checked"> 记住我
			</div>
			</form>
		</div>
		<div class="w3-container w3-border-top w3-padding-16 w3-light-grey">
			<button onclick="document.getElementById('login-modal').style.display='none'" type="button" class="w3-btn w3-red">取消</button>
			<span class="w3-right w3-padding w3-hide-small">忘记 <a href="#">密码?</a></span>
		</div>
	</div>
</div>
<% if (request.getParameter("loginFailed") != null) { %>
<script>
window.onload=function(){
	document.getElementById('login-modal').style.display='block';
}
</script>
<% } %>