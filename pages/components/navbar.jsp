<%--
	Navbar
	BLumia (C) 2016
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<ul class="w3-navbar w3-blue">
	<div class="w3-container">
	<li><a class="w3-indigo" href="index.jsp">课表查询系统</a></li>
	<li><a href="timetable.jsp">查看我的课表</a></li>
	<li><a href="#">要啥自行车</a></li>
	<li><a href="#">待啥手表啊</a></li>
	<% if (hasLoggedin) { %>
		<li class="w3-right w3-dropdown-hover">
			<a href="#"><%=username%></a>
			<div style="right:0" class="w3-dropdown-content w3-white w3-card-4">
				<a href="./api/doLogout.jsp">退出登陆</a>
				<a href="#">你还想干啥</a>
				<a href="#">你觉得你还能干点啥</a>
			</div>
		</li>
	<% } else { %>
		<li onclick="document.getElementById('login-modal').style.display='block'" class="w3-right"><a href="#">登陆系统</a></li>
	<% } %>
	</div>
</ul>