<%--
	Do Login.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.*" %>
<%@ include file="../include/dbConn.inc.jsp" %>

<%
    String[][] users = {{"admin","letmein"},{"中文用户名","password"}};
    boolean flag = false;   
    request.setCharacterEncoding("UTF-8"); 
    String userid = request.getParameter("userid");
    String password = request.getParameter("password");
	String username = "blumia"; // default value
	
	if(request.getMethod().equals("POST")) { 
		String sql = "select * from student_table where student_id = ? and password = ?";
		ps = conn.prepareStatement(sql);
		ps.setString(1,userid);
		ps.setString(2,password);
		rs = ps.executeQuery();
		rs.last();
		if (rs.getRow() == 1) {
			username = rs.getString("student_name");
			flag = true;
		} else {
			flag = false;
		}
	} else {
		flag = false;
	}
	
	// Preparing session data.
	
    if (flag) {
		session.setAttribute("username",username);
		out.print("true");
		response.sendRedirect("../index.jsp");
    } else {
		out.print("false");
		response.sendRedirect("../index.jsp?loginFailed=√");
    }
%>
